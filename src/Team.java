import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Properties;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;



public class Team {
@SuppressWarnings("unused")
public static void main(String[] args) throws IOException {
	//读取网页
	Properties prop = new Properties();
	prop.load(new FileInputStream("./resources/config.properties"));
	//用connect的方法携带cookies连接到云班课,在线解析
	Document doc = Jsoup.connect(prop.getProperty("url")).header("Cookie", prop.getProperty("cookie")).get();
	//获取该班课成员人数
	String stuNumber = doc.getElementById("menu").select("a").get(1).select("span").get(1).text();
	int sn = stuNumber.length()-1;
	int allStuNum = Integer.parseInt(stuNumber.substring(1,sn));
	
	Elements activities=doc.getElementsByClass("interaction-row");
	int classNum=activities.size();//计算全部class的个数
	int j=0;
	int smallAct=0;
	int aver=0;
	//创建对象数组
	Student[] students =new Student[allStuNum];
	for(int i=0;i<classNum;i++) {
		String rows=activities.get(i).select("span").text();

		if(rows.indexOf("课堂完成")!=-1 ) {
			//课堂完成所带的地址
			String attr = activities.get(i).attr("data-url");
			//所有课堂完成部分网站源代码
			Document docSmall = Jsoup.connect(attr).header("Cookie", prop.getProperty("cookie")).get(); 
			Elements mbMgNum=docSmall.getElementsByClass("homework-item");		
			int stuNumAll=mbMgNum.size();
			//第一次给对象数组中的姓名、学号赋值
			if (j==0) {

				for(int mb=0;mb<allStuNum;mb++) {
					String name=mbMgNum.get(mb).select("span").get(0).text();
					String id=mbMgNum.get(mb).select("div").get(4).text();
					students[mb]=new Student(name,id,0);
				}
			j=1;	
			}
			//同个对象 将分数相加
			Elements stuSort=docSmall.getElementsByClass("homework-item");	
			int stuSortSize=stuSort.size();
			for(int m=0;m<stuSortSize;m++) {
				String stuSortText=stuSort.get(m).select("span").text();
				if(stuSortText.indexOf("未提交")!=-1 ||stuSortText.indexOf("尚无评分")!=-1) {}
				else{
					Elements stuScore=docSmall.getElementsByClass("appraised-box cl ");
					String scoreStyle =stuScore.get(m).select("span").get(1).text();
					int score=isNum(scoreStyle);
					aver+=score;
					String name=stuSort.get(m).select("span").get(0).text();
					//判断是否是同个对象,是，则分数相加
					for(int p=0;p<allStuNum;p++) {
						if(name.equals(students[p].getName())) {
							students[p].setScore(score+students[p].getScore());
						}
					}
				}
			}
			
		}
	}

	//讀取文件
	for(int h=0;h<allStuNum;h++)
		System.out.println(students[h]);
	
	Arrays.sort(students);
	File txt = new File("score.txt");
	PrintWriter out = new PrintWriter(new FileWriter(txt));
	String head = "最高经验值为:" + students[0].getScore() + "，" + "最低经验值为:" + students[allStuNum - 1].getScore() + "，"
			+ "平均经验值为:" + aver / allStuNum;
	
	out.print(head + "\r\n");
	for (Student stu : students)
		out.print(stu.toString() + "\r\n");
	out.close();
	
}

//取出分数中内容（数字+分）中的数字
public static int isNum(String str){
	String str1="";
	for(int i=0;i<str.length();i++){
		char chr=str.charAt(i);
		if(chr>=48 && chr<=57)
			str1+=chr;
	}
	int a=Integer.parseInt(str1);
	return a;
}
}

