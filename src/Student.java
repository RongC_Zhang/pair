
public class Student implements Comparable<Student>{
	// 成员变量
	private String name;
	private String id;
	private int score;

	// 构造方法
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student(String name, String id, int score) {
		super();
		this.name = name;
		this.id = id;
		this.score = score;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return id + ", " + name + ", " + score;
	}
	
	public int compareTo(Student stu) {
        int sort1 = stu.score - this.score;
        int sort2 = sort1 == 0 ? Integer.parseInt(this.getId())-Integer.parseInt(stu.getId()):sort1;
        return sort2;
        
	}

}